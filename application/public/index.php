<?php
chdir(dirname(__DIR__));
require_once 'vendor/autoload.php';

use Blackjack\Dealer;
use Blackjack\Deck;

$dealer = new Dealer(new Deck());
$hand = $dealer->dealHand();

$dealer->hitHand($hand);
?>

<pre>

    <?php foreach ($hand->getCards() as $card): ?>

        <?= $card->getName() ?>

    <?php endforeach ?>

    Blackjack : <?= $dealer->hasBlackjack($hand) ? 'YES! \o/' : 'no :(' ?>

</pre>