<?php

namespace Blackjack;

use Blackjack\Card\Rank;
use Blackjack\Card\Suit;

class Deck
{

    /** @var array Card[] */
    private $deck = array();

    public function __construct()
    {
        /** @var Suit\SuitInterface[] $suits */
        $suits = array(
            new Suit\Spades(),
            new Suit\Hearts(),
            new Suit\Clubs(),
            new Suit\Diamonds(),
        );

        /** @var Rank\RankInterface[] $ranks */
        $ranks = array(
            new Rank\Ace(),
            new Rank\Two(),
            new Rank\Three(),
            new Rank\Four(),
            new Rank\Five(),
            new Rank\Six(),
            new Rank\Seven(),
            new Rank\Eight(),
            new Rank\Nine(),
            new Rank\Ten(),
            new Rank\Jack(),
            new Rank\Queen(),
            new Rank\King(),
        );

        foreach ($suits as $suit) {
            foreach ($ranks as $rank) {
                $this->deck[$rank->getName() . '-of-' . $suit->getName()] = new Card($rank, $suit);
            }
        }
    }

    public function shuffle()
    {
        $keys = array_keys($this->deck);
        shuffle($keys);
        $random = array();
        foreach ($keys as $key) {
            $random[$key] = $this->deck[$key];
        }

        $this->deck = $random;
    }

    /**
     * @return Card
     */
    public function draw()
    {
        return array_shift($this->deck);
    }

}
