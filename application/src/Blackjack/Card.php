<?php

namespace Blackjack;

use Blackjack\Card\Rank\RankInterface;
use Blackjack\Card\Suit\SuitInterface;

class Card
{
    /** @var RankInterface */
    protected $rank;

    /** @var SuitInterface */
    protected $suit;

    /**
     * @param RankInterface $rank
     * @param SuitInterface $suit
     */
    public function __construct(RankInterface $rank, SuitInterface $suit)
    {
        $this->rank = $rank;
        $this->suit = $suit;
    }

    /**
     * @return array Array of values.
     */
    public function getValues()
    {
        return $this->rank->getValue();
    }

    /**
     * @return string Name of card.
     */
    public function getName()
    {
        return ucfirst($this->rank->getName()) . ' of ' . ucfirst($this->suit->getName());
    }
}