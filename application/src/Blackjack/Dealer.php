<?php

namespace Blackjack;

class Dealer
{

    /** @var Deck */
    private $deck;

    /**
     * @param Deck $deck
     */
    public function __construct(Deck $deck)
    {
        $deck->shuffle();
        $this->deck = $deck;
    }

    /**
     * Draws two cards from the deck to the hand.
     *
     * @return Hand
     */
    public function dealHand()
    {
        return new Hand(array(
            $this->deck->draw(),
            $this->deck->draw(),
        ));
    }

    /**
     * Deals a card from the deck to the hand
     *
     * @param Hand $hand
     */
    public function hitHand(Hand $hand)
    {
        $hand->addCard($this->deck->draw());
    }

    /**
     * @param Hand $hand
     */
    public function hasBlackjack(Hand $hand)
    {
        foreach ($hand->getCounts() as $sum) {
            if ($sum === 21) {
                return true;
            }
        }

        return false;
    }

}
