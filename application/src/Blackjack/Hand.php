<?php

namespace Blackjack;

class Hand
{

    /** @var Card[] */
    protected $cards;

    /** @var array */
    protected $counts = array(0);

    /**
     * @param array $cards
     */
    public function __construct(array $cards)
    {
        foreach ($cards as $card) {
            $this->addCard($card);
        }
    }

    /**
     * @param Card $card
     */
    public function addCard(Card $card)
    {
        $this->cards[] = $card;
         $this->countCard($card);
    }

    /**
     * Get all cards in hand.
     *
     * @return Card[]
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * Gets the hand possible counts.
     *
     * @return array
     */
    public function getCounts()
    {
        return $this->counts;
    }

    /**
     * Takes an array of possible card counts, and adds on the new card's values.
     *
     * @param Card $card
     * @return array
     */
    private function countCard(Card $card)
    {
        $newSums = array();

        foreach ($this->counts as $count) {
            foreach ($card->getValues() as $value) {
                $newSums[] = $count + $value;
            }
        }

        $this->counts =  $newSums;
    }

}
