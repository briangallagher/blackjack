<?php

namespace Blackjack\Card\Rank;

class RankAbstract implements RankInterface
{
    public function getValue()
    {
        return $this->value;
    }

    public function getName()
    {
        return $this->name;
    }
}