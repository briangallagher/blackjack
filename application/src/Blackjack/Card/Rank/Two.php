<?php

namespace Blackjack\Card\Rank;

class Two extends RankAbstract{
    protected $value = array(
        2
    );

    protected $name = 'two';
}
