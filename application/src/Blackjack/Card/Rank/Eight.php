<?php

namespace Blackjack\Card\Rank;

class Eight extends RankAbstract{
    protected $value = array(
        8
    );

    protected $name = 'eight';
}
