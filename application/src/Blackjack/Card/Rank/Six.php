<?php

namespace Blackjack\Card\Rank;

class Six extends RankAbstract{
    protected $value = array(
        6
    );

    protected $name = 'six';
}
