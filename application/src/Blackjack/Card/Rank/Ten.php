<?php

namespace Blackjack\Card\Rank;

class Ten extends RankAbstract{
    protected $value = array(
        10
    );

    protected $name = 'ten';
}
