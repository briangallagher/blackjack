<?php
/**
 * Created by JetBrains PhpStorm.
 * User: brian
 * Date: 27/11/2013
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */

namespace Blackjack\Card\Rank;


class King extends RankAbstract {

    protected $value = array(
        10
    );

    protected $name = 'king';
}