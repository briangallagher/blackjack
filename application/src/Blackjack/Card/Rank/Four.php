<?php

namespace Blackjack\Card\Rank;

class Four extends RankAbstract{
    protected $value = array(
        4
    );

    protected $name = 'four';
}
