<?php

namespace Blackjack\Card\Rank;

class Queen extends RankAbstract{
    protected $value = array(
        10
    );

    protected $name = 'queen';
}
