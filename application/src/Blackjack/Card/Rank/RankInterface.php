<?php

namespace Blackjack\Card\Rank;

interface RankInterface {
    function getValue();

    function getName();
}