<?php

namespace Blackjack\Card\Rank;

class Three extends RankAbstract{
    protected $value = array(
        3
    );

    protected $name = 'three';
}
