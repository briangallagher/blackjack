<?php

namespace Blackjack\Card\Rank;

class Nine extends RankAbstract{
    protected $value = array(
        9
    );

    protected $name = 'nine';
}
