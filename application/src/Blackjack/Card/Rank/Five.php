<?php

namespace Blackjack\Card\Rank;

class Five extends RankAbstract{
    protected $value = array(
        5
    );

    protected $name = 'five';
}
