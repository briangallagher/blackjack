<?php

namespace Blackjack\Card\Rank;

class Jack extends RankAbstract{
    protected $value = array(
        10
    );

    protected $name = 'jack';
}
