<?php

namespace Blackjack\Card\Rank;

class Seven extends RankAbstract{
    protected $value = array(
        7
    );

    protected $name = 'seven';
}
