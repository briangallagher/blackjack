<?php

namespace Blackjack\Card\Rank;

class Ace extends RankAbstract{
    protected $value = array(
        1,
        11
    );

    protected $name = 'ace';
}