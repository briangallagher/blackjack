<?php

namespace Blackjack\Card\Suit;

interface SuitInterface
{
    function getName();
}
