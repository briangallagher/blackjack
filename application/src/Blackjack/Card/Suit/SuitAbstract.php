<?php

namespace Blackjack\Card\Suit;

abstract class SuitAbstract implements SuitInterface{
    public function getName()
    {
        return $this->name;
    }
}
